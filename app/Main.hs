module Main where

import Prelude hiding (getLine)
import Lib
import qualified Data.Vector as V
import Data.List (unfoldr, sort)
import Data.List.Extra (anySame, chunksOf, (\\))
import Data.Tuple
import Data.Maybe
import Control.Applicative

main :: IO ()
main = someFunc


-- n is dimensions, m^2 is the length of a dimension
toIndex m loc = foldr (\acc cur -> acc+(m^2)*cur) 0 loc

unIndex n m index = take n $ unfoldr (\i -> Just (swap (i `divMod` (m^2)))) index


-- getLine::Int -> Int -> [Int] -> [[Int]]
getLine m dim loc = [ (take dim loc)++[x]++(drop (dim+1) loc) | x <- [0..(m^2-1)] ]

getCube :: Int -> [Int] -> [[Int]]
getCube m loc = cube
   where cube = map (zipWith (+) baseLoc) baseCube
         baseLoc = map (\l -> m*(l `div` m)) loc
         baseCube = genCombos m (length loc)

genCombos _ 0 = [[]]
genCombos max 1 = [[x] | x <- [0..max-1]]
genCombos max length = [ x:g | x <- [0..max-1], g <- genCombos max (length-1) ]

getLayers m n d = [map (\l -> (take d l)++[x]++(drop d l)) combos | x <- [0..m^2-1]]
   where combos = genCombos (m^2) (n-1)

checkCube f board m loc = checkLocsAreUnique board m f (getCube m loc)

checkLine f board m dim loc = checkLocsAreUnique board m f (getLine m dim loc)

checkLocsAreUnique board m f locs = not $ anySame $ getUnder f board m locs

getUnder f board m locs = map (\l -> f (board V.! (toIndex m l))) locs

-- checkPuzzle puzzle _ m 2 = 
-- checkPuzzle puzzle puzzleFuncs m n = map checkSubPuzzle subPuzzles
-- checkPuzzle puzzle puzzleFuncs m n = checkThisDimension

data CorrectOrErrors b a = Empty
                         | Correct a
                         | Errors [b]

instance Semigroup (CorrectOrErrors b a) where
   Empty <> x = x
   x <> Empty = x
   Correct a <> Errors bs = Errors bs
   Correct a <> Correct b = Correct a
   Errors as <> Correct b = Errors as
   Errors as <> Errors bs = Errors (as++bs)

instance Monoid (CorrectOrErrors b a) where
   mempty = Empty

checkPuzzle _ _ a _ 1 l = [("end",(a,1,l),True)]
checkPuzzle board boardFunc arg m 2 l = ("checkLines",(arg,2,l),checkLines board (boardFunc arg) m 2):("checkCubes",(arg,2,l),checkCubes):[]
   where checkCubes = and $ map (checkCube (boardFunc arg) board m) cubeLocs
         cubeLocs = map (\l -> map (*m) l) $ genCombos m 2
checkPuzzle board boardFunc arg m n l = ("checkCubes",(arg,n,l),checkCubes):checkLayers --checkLines && checkCubes && checkLayers
   where checkCubes = and $ map (checkCube (boardFunc arg) board m) cubeLocs
         cubeLocs = map (\l -> map (*m) l) $ genCombos m n
         checkLayers = do --and $ do
            d <- ([0..n-1] \\ arg)
            (ln, layer) <- zip [0..] $ getLayers m n d
            let newPuzzle = V.fromList $ getUnder (boardFunc (d:arg)) board m layer
            --return $ checkPuzzle newPuzzle boardFunc (d:arg) m (n-1)
            checkPuzzle newPuzzle boardFunc (d:arg) m (n-1) ln


checkLines board func m n = and $ map (\d -> and $ map (checkLine func board m d) (head $ getLayers m n d)) [0..n-1]

checkLayers board boardFunc arg m n = do
   d <- ([0..n-1] \\ arg)
   layer <- getLayers m n d
   let newPuzzle = V.fromList $ getUnder (boardFunc (d:arg)) board m layer
   return $ checkPuzzle newPuzzle boardFunc (d:arg) m (n-1)


pairsToFunc a pairs = fromMaybe a (lookup a pairs)

sudoku3example = 
            V.fromList [1,2,3,4
                       ,3,4,1,2
                       ,2,1,4,3
                       ,4,3,2,1

                       ,5,6,7,8
                       ,7,8,5,6
                       ,6,5,8,7
                       ,8,7,6,5

                       ,4,3,2,1
                       ,2,1,4,3
                       ,3,4,1,2
                       ,1,2,3,4

                       ,8,7,6,5
                       ,6,5,8,7
                       ,7,8,5,6
                       ,5,6,7,8]

-- 0 is row, 1 is col, 2 is layer
sudoku3exampleFuncs [0] v = pairsToFunc v [(5,2),(6,1),(7,4),(8,3)]
--sudoku3exampleFuncs [1] v = pairsToFunc v [(5,2),(6,1),(7,4),(8,3)]
sudoku3exampleFuncs [1] v = pairsToFunc v [(5,3),(6,4),(7,1),(8,2)]
sudoku3exampleFuncs [2] v = pairsToFunc v [(5,2),(6,1),(7,4),(8,3)]
sudoku3exampleFuncs _ v = v

sudoku4example = 
   V.fromList [1,2,3,4  ,13,14,15,16   ,2,1,4,3 ,14,13,16,15
              ,3,4,1,2  ,15,16,13,14   ,4,3,2,1 ,16,15,14,13
              ,2,1,4,3  ,14,13,16,15   ,1,2,3,4 ,13,14,15,16
              ,4,3,2,1  ,16,15,14,13   ,3,4,1,2 ,15,16,13,14

              ,5,6,7,8  ,9,10,11,12 ,6,5,8,7 ,10,9,12,11
              ,7,8,5,6  ,11,12,9,10 ,8,7,6,5 ,12,11,10,9
              ,6,5,8,7  ,10,9,12,11 ,5,6,7,8 ,9,10,11,12
              ,8,7,6,5  ,12,11,10,9 ,7,8,5,6 ,11,12,9,10

              ,3,4,1,2  ,15,16,13,14   ,4,3,2,1 ,16,15,14,13
              ,1,2,3,4  ,13,14,15,16   ,2,1,4,3 ,14,13,16,15
              ,4,3,2,1  ,16,15,14,13   ,3,4,1,2 ,15,16,13,14
              ,2,1,4,3  ,14,13,16,15   ,1,2,3,4 ,13,14,15,16

              ,7,8,5,6  ,11,12,9,10 ,8,7,6,5 ,12,11,10,9
              ,5,6,7,8  ,9,10,11,12 ,6,5,8,7 ,10,9,12,11
              ,8,7,6,5  ,12,11,10,9 ,7,8,5,6 ,11,12,9,10
              ,6,5,8,7  ,10,9,12,11 ,5,6,7,8 ,9,10,11,12 ]

--------0 hyper, 1 layer, 2 row, 3 collumn
------1 hyper, 3 layer, 0 row, 2 collumn
---- 0 row, 1 hyper, 2 collumn, 3 layer
--0 collumn, 1 hyper, 2 row, 3 layer
sudoku4exampleFuncs [0] v = pairsToFunc v [(9,2),(10,3),(11,4),(12,5),(13,6),(14,7),(15,8),(16,1)]
sudoku4exampleFuncs [1] v = pairsToFunc v [(9,1),(10,2),(11,3),(12,4),(13,5),(14,6),(15,7),(16,8)]
sudoku4exampleFuncs [2] v = pairsToFunc v [(9,7),(10,8),(11,1),(12,2),(13,3),(14,4),(15,5),(16,6)]
sudoku4exampleFuncs [3] v = pairsToFunc v [(9,1),(10,2),(11,3),(12,4),(13,5),(14,6),(15,7),(16,8)]
-- read these right to left
sudoku4exampleFuncs [1,0] v = pairsToFunc v [(5,4),(6,3),(7,2),(8,1)]
sudoku4exampleFuncs [2,0] v = pairsToFunc v [(5,4),(6,1),(7,2),(8,3)]
sudoku4exampleFuncs [3,0] v = pairsToFunc v [(5,1),(6,2),(7,3),(8,4)]
sudoku4exampleFuncs [0,1] v = pairsToFunc v [(5,4),(6,1),(7,2),(8,3)]
sudoku4exampleFuncs [2,1] v = pairsToFunc v [(5,4),(6,3),(7,2),(8,1)]
sudoku4exampleFuncs [3,1] v = pairsToFunc v [(5,1),(6,2),(7,3),(8,4)]
sudoku4exampleFuncs [0,2] v = pairsToFunc v [(5,4),(6,3),(7,2),(8,1)]
sudoku4exampleFuncs [1,2] v = pairsToFunc v [(5,3),(6,4),(7,1),(8,2)]
sudoku4exampleFuncs [3,2] v = pairsToFunc v [(5,1),(6,2),(7,3),(8,4)]
sudoku4exampleFuncs [0,3] v = pairsToFunc v [(5,4),(6,3),(7,2),(8,1)]
sudoku4exampleFuncs [1,3] v = pairsToFunc v [(5,1),(6,2),(7,3),(8,4)]
sudoku4exampleFuncs [2,3] v = pairsToFunc v [(5,4),(6,3),(7,2),(8,1)]
sudoku4exampleFuncs [] v = v
-- sudoku4exampleFuncs _ v = v





a .> b = b a

a .:. b = (a,b)

