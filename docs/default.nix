with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    R
    which # Required by Packrat
    (texlive.combine {
      inherit (pkgs.texlive) scheme-small comment marvosym ucs relsize mathtools tikz-cd listings pgf lm;
    })
  ];
}
